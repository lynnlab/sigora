**sigora: Signature Overrepresentation Analysis**

Pathway Analysis is the process of statistically linking observations on the molecular level to biological processes or pathways on the systems (organism, organ, tissue, cell) level. Traditionally, pathway analysis methods regard pathways as collections of single genes and treat all genes in a pathway as equally informative. This can lead to identification of spurious (misleading) pathways as statistically significant, since components are often shared amongst pathways. SIGORA seeks to avoid this pitfall by focusing on genes or gene-pairs that are (as a combination) specific to a single pathway. In relying on such pathway gene-pair signatures (Pathway-GPS), SIGORA inherently uses the status of other genes in the experimental context to identify the most relevant pathways. The current version allows for pathway analysis of human and mouse data sets and contains pre-computed Pathway-GPS data for pathways in the KEGG and Reactome pathway repositories as well as mechanisms for extracting GPS for user supplied repositories.


Version:	2.0.1

Depends:	R (≥ 2.10)

Imports:	utils, stats

Suggests:	slam

Published:	2016-05-11

Author:	Amir B.K. Foroushani, Fiona S.L. Brinkman, David J. Lynn

Maintainer:	Amir Foroushani <sigora.dev at gmail.com>

License:	GPL-2

NeedsCompilation:	no

Citation:	sigora citation info

CRAN checks:	sigora results
Reference manual:	sigora.pdf

Package source:	sigora_2.0.1.tar.gz

Windows binaries:	r-devel: sigora_2.0.1.zip, r-release: sigora_2.0.1.zip, r-oldrel: sigora_2.0.1.zip

OS X binaries:	r-release: sigora_2.0.1.tgz, r-oldrel: sigora_2.0.1.tgz

Old sources:	sigora archive

Linking:

Please use the canonical form https://CRAN.R-project.org/package=sigora to link to this page.
